import { runUsingViewports } from '../Infrastructure/viewport';

runUsingViewports(async (page, index) => {
    await page.goto('https://google.com');
    await page.type('input[name=q]', 'testing', {delay: 200});
    await page.screenshot({path: index + '.png'});
});