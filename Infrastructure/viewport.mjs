/* Define Viewports Here */

export const resizeLarge = async (page) => {
    await page.setViewport({
        height: 1080,
        width: 1920
    });
}

export const resizeMedium = async (page) => {
    await page.setViewport({
        height: 1080,
        width: 1920
    });
}

export const resizeSmall = async (page) => {
    await page.setViewport({
        height: 1080,
        width: 1920
    });
}

export const resizeMobile = async (page) => {
    await page.setViewport({
        height: 812,
        width: 375,
        isMobile: true
    });
}