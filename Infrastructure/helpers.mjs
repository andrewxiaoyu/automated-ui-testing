import puppeteer from 'puppeteer';
import { url } from '../Options/global';
import {
    resizeLarge,
    resizeMedium,
    resizeSmall,
    resizeMobile
} from './viewport';

const viewports = [
    resizeLarge,
    resizeMedium,
    resizeSmall,
    resizeMobile
]

export const runUsingViewports = async (page, test) => {
    viewports.forEach(async (resize, index)=>{
        await resize(page);
        await test(page, index);
        
    });
}

export const login = async (page, user) => {
    await page.goto(url);
    let logonButton = await page.$x("//a[@class='btn btn-primary mb-sm-2' and text()='Logon']");
    if(logonButton.length > 0) await logonButton[0].click();
    await page.waitForNavigation();
}

export const testWrapper = async (test, debug = false) => {
    let browser = await puppeteer.launch( debug ? {headless: false} : {} );
    let page = await browser.newPage();
    await login(page, 'hello');
    await test();
    await browser.close();
}